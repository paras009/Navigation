package org.tracker.prashu.TrackFootsMap;

import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.tracker.prashu.rastaNapo.R;

import static org.tracker.prashu.TrackFootsMap.MapsActivity.strSeparator;

public class SavedMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private MarkerOptions markerOptions;
    public String id;
    public String name;
    public String startLat;
    public String startLon;
    public String currentLat;
    public String currentLon;
    public String polylineArray;
    public String duration;
    public String distance;
    public String color;
    String[] polylinesArr;


    TextView tvName;
    TextView tvDuration;
    TextView tvDistance;
    FloatingActionButton backButtonOnSavedMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_map1);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tvName = (TextView)findViewById(R.id.savedMapName);
        tvDistance = (TextView)findViewById(R.id.savedMapDistance);
        tvDuration = (TextView)findViewById(R.id.savedMapDuration);
        backButtonOnSavedMap = (FloatingActionButton)findViewById(R.id.backButtonOnSavedMap);

        backButtonOnSavedMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        id = getIntent().getStringExtra("id");
        name = getIntent().getStringExtra("name");
        startLat = getIntent().getStringExtra("startLat");
        startLon = getIntent().getStringExtra("startLon");
        currentLat = getIntent().getStringExtra("currentLat");
        currentLon = getIntent().getStringExtra("currentLon");
        polylineArray = getIntent().getStringExtra("polylineArray");
        duration = getIntent().getStringExtra("duration");
        distance = getIntent().getStringExtra("distance");
        color = getIntent().getStringExtra("color");


        polylinesArr = convertStringToArray(polylineArray);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        int colo = Color.parseColor(color);


        createCustomMarker(startLat, startLon, "Source",BitmapDescriptorFactory.HUE_RED);
        createCustomMarker(currentLat, currentLon, "Destination",BitmapDescriptorFactory.HUE_AZURE);
        createPath(polylinesArr);





        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                LatLng startLatlang = new LatLng(Double.parseDouble(startLat), Double.parseDouble(startLon));
                LatLng currentLatlang = new LatLng(Double.parseDouble(currentLat), Double.parseDouble(currentLon));
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(startLatlang);
                builder.include(currentLatlang);
                LatLngBounds bounds = builder.build();
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 400));
                mMap.setMaxZoomPreference(19.0f);

            }
        });



        tvName.setText(name);
        tvDuration.setText("Duration: "+duration+"sec");
        tvDistance.setText("Distance: "+distance+"m    ");
    }


    public void createCustomMarker(String latitute, String longitude, String title, Float color) {

        LatLng latLng = new LatLng(Double.parseDouble(latitute), Double.parseDouble(longitude));
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(title);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(color));
        mMap.addMarker(markerOptions);
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f));
    }


    public void createPath(String[] polylinePaths) {
        for (int k = 0; k < polylinePaths.length; k++) {
            PolylineOptions options = new PolylineOptions();
            options.color(Color.parseColor(color));
            options.width(13);
            options.addAll(PolyUtil.decode(polylinePaths[k]));

            mMap.addPolyline(options);
        }
    }

    public static String[] convertStringToArray(String str){
        String[] arr = str.split(strSeparator);
        return arr;
    }
}
