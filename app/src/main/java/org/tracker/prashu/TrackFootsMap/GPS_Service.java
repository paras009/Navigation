package org.tracker.prashu.TrackFootsMap;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;


public class GPS_Service extends Service implements GoogleApiClient.ConnectionCallbacks {

    public LocationListener locationListener;
    public static GoogleApiClient mClient;
    private LocationRequest locationRequest;
    public final int REQUEST_CODE = 0;
    public final int FLAG = 0;
    String minDistanceChangeFromSettings = "1";

    public GPS_Service() {
    }

    protected synchronized void buildGoogleApiClient() {
        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        mClient.disconnect();
    }

    @Override
    public void onCreate() {
        super.onCreate();



        buildGoogleApiClient();
        mClient.connect();

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Intent intent = new Intent("GPS_location_update");
                intent.putExtra("myLocation", location);
                sendBroadcast(intent);
            }
        };
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        minDistanceChangeFromSettings = String.valueOf(intent.getExtras().get("minDistanceChangeFromSettings"));
        Toast.makeText(this, ""+minDistanceChangeFromSettings, Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(10);
        locationRequest.setFastestInterval(10);
        locationRequest.setSmallestDisplacement(Integer.parseInt(minDistanceChangeFromSettings));
        locationRequest.setMaxWaitTime(30000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo != null) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mClient, locationRequest, locationListener);
        } else {
            Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}




