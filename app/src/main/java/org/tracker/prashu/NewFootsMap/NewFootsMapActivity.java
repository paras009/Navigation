package org.tracker.prashu.NewFootsMap;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tracker.prashu.TrackFootsMap.DataInterface;
import org.tracker.prashu.TrackFootsMap.FetchMapData;
import org.tracker.prashu.TrackFootsMap.MapsActivity;
import org.tracker.prashu.rastaNapo.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewFootsMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        DataInterface{


    private static final String TAG = "MapActivity";
    private String baseUrl = "https://maps.googleapis.com/maps/api/directions/json?";
    private String origin = "origin";
    private String destination = "destination";
    private String key = "key";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
            new LatLng(-40, -168), new LatLng(71, 136));


    private AutoCompleteTextView autoCompleteTextView;
    private ImageButton imageButton;
    private Button resetBtn;
    private CoordinatorLayout coordinatorLayout;

    //vars
    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private GoogleApiClient mGoogleApiClient;
    MarkerOptions markerOptions;

    private double destinationLatitude;
    private double destinationLongitude;
    private double sourceLatitude;
    private double sourceLongitude;
    private String destinationAddress;
    private int destinationCounter = 0;

    String[] polylines;
    String encodedPolylines = "";
    StringBuffer sb;
    String directionUrl;
    static String[] polylinePaths;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_foots_map);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.input_search);
        imageButton = (ImageButton) findViewById(R.id.ic_magnify);
        resetBtn = (Button)findViewById(R.id.resetBtn);
        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.clear();
                Toast.makeText(NewFootsMapActivity.this, "New Fresh Foots Map.", Toast.LENGTH_SHORT).show();
            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tasksToExecuteOnSearchClicked();
            }
        });
        getLocationPermission();

    }

    private void tasksToExecuteOnSearchClicked() {

        Toast.makeText(NewFootsMapActivity.this, "Searching...", Toast.LENGTH_SHORT).show();
        getDeviceLocation();
        geoLocate();

        hideSoftKeyboard();

        directionUrl = baseUrl + origin + "=" + sourceLatitude + "," + sourceLongitude + "&"
                + destination + "=" + destinationLatitude + "," + destinationLongitude + "&"
                + key + "=" + getString(R.string.google_api_key_paras);

        new FetchMapData(NewFootsMapActivity.this, directionUrl, NewFootsMapActivity.this).execute();

    }

    private void boundCameraScreen() {
        LatLng destinationLatLng = new LatLng(destinationLatitude, destinationLongitude);
        LatLng sourceLatLang = new LatLng(sourceLatitude, sourceLongitude);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(sourceLatLang);
        builder.include(destinationLatLng);
        LatLngBounds bounds = builder.build();

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
        mMap.setMaxZoomPreference(19.0f);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (mLocationPermissionsGranted) {
            getDeviceLocation();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);

            init();
        }
    }

    private void init() {
        Log.d(TAG, "init: initializing");

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient,
                LAT_LNG_BOUNDS, null);

        autoCompleteTextView.setAdapter(mPlaceAutocompleteAdapter);

        autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {

                    //execute my method for searching
                    tasksToExecuteOnSearchClicked();
                }

                return false;
            }
        });

        hideSoftKeyboard();
    }

    private void geoLocate() {
        Log.d(TAG, "geoLocate: geolocating");

        String searchString = autoCompleteTextView.getText().toString();

        if (!searchString.isEmpty()){
            Geocoder geocoder = new Geocoder(NewFootsMapActivity.this);
            List<Address> list = new ArrayList<>();
            try {
                list = geocoder.getFromLocationName(searchString, 1);
            } catch (IOException e) {
                Log.e(TAG, "geoLocate: IOException: " + e.getMessage());
            }

            if (list.size() > 0) {
                Address address = list.get(0);

                destinationAddress = address.getAddressLine(0);
                destinationLatitude = address.getLatitude();
                destinationLongitude = address.getLongitude();

                Log.d(TAG, "geoLocate: found a location: " + address.toString());
                //Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show();

               // moveCamera(new LatLng(destinationLatitude, destinationLongitude), DEFAULT_ZOOM, address.getAddressLine(0));

            }
        }else {
            destinationLatitude = sourceLatitude;
            destinationLongitude = sourceLongitude;
            Toast.makeText(this, "Source = Destination", Toast.LENGTH_SHORT).show();
        }
    }



    public String getPath(JSONObject googlePathJson) {
        String polyline = null;
        try {
            polyline = googlePathJson.getJSONObject("polyline").getString("points");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return polyline;
    }



    public String[] getPaths(JSONArray googleStepsJson) {
        int count = googleStepsJson.length();

        sb = new StringBuffer("");
        polylines = new String[count];

        for (int i = 0; i < count; i++) {
            try {
                polylines[i] = getPath(googleStepsJson.getJSONObject(i));
                sb.append("enc:" + polylines[i] + ":");
                if (i < count - 1) {
                    sb.append("|");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        encodedPolylines = sb.toString();

        return polylines;
    }


    public void createPath(String[] polylinePaths) {
        for (int k = 0; k < polylinePaths.length; k++) {
            PolylineOptions options = new PolylineOptions();
            options.color(Color.parseColor("#000000"));
            options.width(13);
            options.addAll(PolyUtil.decode(polylinePaths[k]));

            mMap.addPolyline(options);
        }
    }


    private void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: getting the devices current location");

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: found location!");
                            Location currentLocation = (Location) task.getResult();
                            sourceLatitude = currentLocation.getLatitude();
                            sourceLongitude = currentLocation.getLongitude();

                        } else {
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(NewFootsMapActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
        }
    }

    private void moveCamera(LatLng latLng, float zoom, String title) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        if (!title.equals("My Location")) {
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mMap.addMarker(options);
        }

        hideSoftKeyboard();
    }

    public void createCustomMarker(Double latitude, Double longitude, String title, Float markerColor) {

        //BitmapDescriptorFactory.HUE_GREEN

        LatLng currentlatLng = new LatLng(latitude, longitude);
        /*LatLng startLatLang = new LatLng(sourceLatitude, sourceLongitude);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(startLatLang);
        builder.include(currentlatLng);
        LatLngBounds bounds = builder.build();*/


        markerOptions = new MarkerOptions();
        markerOptions.position(currentlatLng);
        markerOptions.title(title);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(markerColor));
        mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentlatLng, 16f));
        //mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));
    }










    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(NewFootsMapActivity.this);
    }

    private void getLocationPermission() {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(this,
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    //initialize our map
                    initMap();
                }
            }
        }
    }

    private void hideSoftKeyboard() {
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    @Override
    public void fetchedData(String data) {

        try {
            if (data != null) {
                JSONObject mainJosnObj = new JSONObject(data);
                String parsingStatus = mainJosnObj.getString("status");

                if (parsingStatus.equals("OK")) {
                    JSONArray routesJsonArray = mainJosnObj.getJSONArray("routes");

                    JSONArray legsJsonArray = routesJsonArray.getJSONObject(0).getJSONArray("legs");

                    JSONArray stepsJsonArray = legsJsonArray.getJSONObject(0).getJSONArray("steps");

                    polylinePaths = getPaths(stepsJsonArray);


                } else if (parsingStatus.equals("REQUEST_DENIED")) {
                    Toast.makeText(this, "REQUEST_DENIED", Toast.LENGTH_SHORT).show();
                }

                createCustomMarker(sourceLatitude, sourceLongitude, "Source", BitmapDescriptorFactory.HUE_RED);
                createCustomMarker(destinationLatitude, destinationLongitude, destinationAddress, BitmapDescriptorFactory.HUE_AZURE);
                createPath(polylinePaths);
                boundCameraScreen();
                Snackbar.make(coordinatorLayout, "New Foots Plotted on Map !!", Snackbar.LENGTH_LONG).show();

            }
        } catch (Exception e) {
            Toast.makeText(this, "Exception: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        //textView.setText("Location: " + distanceValue + "m , " + durationValue + "sec");
    }
}