package org.tracker.prashu.rastaNapo;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

// class used to access the device's database to store the list of favourite and watchlist movies.
// this class extends the SQLiteOpenHelper class.
public class MyDatabaseHelper extends SQLiteOpenHelper {

    // final variables that are used in the tables.
    public static final String TAG = "prashu database";
    private static final String DATABASE_NAME = "Test.db";
    public static final int DATABASE_VERSION = 4;
    public static final String TABLE_NAME = "testTable";

    public static final String ID = "_id";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String START_LAT = "start_latitiude";
    public static final String START_LONG = "start_longitude";
    public static final String CURRENT_LAT = "current_latitude";
    public static final String CURRENT_LONG = "current_longitude";
    public static final String POLYLINE_ARRAY = "polyline_array";
    public static final String DURATION = "duration";
    public static final String DISTANCE = "distance";
    public static final String COLOR = "color";


    // final strings that creates the table and delete teh table.
    public static final String CREATE_TABLE = " create table " + TABLE_NAME +
            " (" + ID + " integer primary key autoincrement unique, "
            + EMAIL + " varchar(255), "
            + NAME + " varchar(255), "
            + DATE + " varchar(255), "
            + TIME + " varchar(255), "
            + START_LAT + " varchar(255), "
            + START_LONG + " varchar(255), "
            + CURRENT_LAT + " varchar(255), "
            + CURRENT_LONG + " varchar(255), "
            + POLYLINE_ARRAY + " varchar, "
            + DURATION + " varchar(255), "
            + DISTANCE + " varchar(255),"
            + COLOR + " varchar(255)); ";


    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    public static Context dbContext;

    //constructor to set the database name, database version, factory value, and context.
    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.dbContext = context;
        Log.i(TAG, " Constructor called!!!!!!!!!!!!!!!!!!!!!");
    }

    // overridden method of the SQLiteOpenHelper class that is called on create of the database.
    // here i used to create the table.
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE);
            Log.i(TAG, "inside create!!!!!!!!!!!!!!!!");
        } catch (SQLException e) {
            Log.i(TAG, "ERR!!!!!!!!!!!!!!!!!!!!");
        }
    }

    // overridden method of the SQLiteOpenHelper class that is called on upgrade of the database.
    // here i deleted the old table and created a new table.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE);
            Log.i(TAG, "inside upgrade!!!!!!!!!!!!!!!!!!!!!");
            onCreate(db);
        } catch (SQLException e) {
            Log.i(TAG, "ERR!!!!!!!!!!!!!!");
        }
    }
}
