package org.tracker.prashu.rastaNapo;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import yuku.ambilwarna.AmbilWarnaDialog;

public class MySettingsActivity extends AppCompatActivity {

    int defaultColor;
    Button button;
    Button settingsSaveButton;
    NumberPicker numberPicker;
    String hexColor;
    public static final int SETTINGS_REQUEST = 102;
    Integer minDistanceChangeFromSettings;
    private Toolbar mySettingsToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_settings);
        defaultColor = ContextCompat.getColor(this, R.color.colorBlack);
        hexColor = String.format("#%06X", (0xFFFFFF & defaultColor));

        button = (Button) findViewById(R.id.changePathColorBtn);
        settingsSaveButton = (Button) findViewById(R.id.settingsSaveButton);

        mySettingsToolbar = (Toolbar) findViewById(R.id.app_bar_settings);
        mySettingsToolbar.setTitle("Settings");
        mySettingsToolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(mySettingsToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        numberPicker = (NumberPicker) findViewById(R.id.settingsNumberPicker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(500);
        numberPicker.setWrapSelectorWheel(true);

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                //Display the newly selected number from picker
                minDistanceChangeFromSettings = newVal;
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectColorPick();

            } // onclick ends
        });

        settingsSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("selectedColorStringKey", hexColor);
                intent.putExtra("minDistanceChangeFromSettings", minDistanceChangeFromSettings);
                setResult(SETTINGS_REQUEST, intent);
                finish();
            }
        });
    }

    public void selectColorPick() {
        final AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(
                this,
                defaultColor,
                new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {
                    }

                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {
                        defaultColor = color;

                        hexColor = String.format("#%06X", (0xFFFFFF & defaultColor));

                    }
                });
        colorPicker.show();
    }
}
