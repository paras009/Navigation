package org.tracker.prashu.rastaNapo;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.tracker.prashu.NewFootsMap.NewFootsMapActivity;
import org.tracker.prashu.TrackFootsMap.MapsActivity;
import org.tracker.prashu.savedRoutes.SavedRoutesActivity;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.lang.reflect.Method;
import java.util.Arrays;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    TextView tvUserWelcome1;
    TextView tvUserWelcome2;
    TextView tvUserWelcome3;
    private Toolbar myToolbar;

    FloatingActionButton fabQuestionMarkHint;
    Button startBtn, savedBtn, newBtn, SettingsBtn;
    public static final int RC_SIGN_IN = 1;
    public static final int PERMISSION_REQUEST_CODE = 101;
    public static final int SETTINGS_REQUEST = 102;
    private CoordinatorLayout coordinatorLayout;
    public String userName;
    public String userEmail;
    String userDefinedColor;
    Integer minDistanceChangeFromSettings = 1;
    private long back_pressed;
    boolean mobileDataEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        userDefinedColor = "#000000";

        tvUserWelcome1 = (TextView) findViewById(R.id.welcomeUserText);
        tvUserWelcome2 = (TextView) findViewById(R.id.welcomeUserText2);
        tvUserWelcome3 = (TextView) findViewById(R.id.welcomeUserText3);
        fabQuestionMarkHint = (FloatingActionButton) findViewById(R.id.fabQuestionMarkHint);
        myToolbar = (Toolbar) findViewById(R.id.app_bar);
        myToolbar.setTitle("Rasta Napo");
        myToolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        myToolbar.setTitleMargin(100, 0, 0, 0);
        setSupportActionBar(myToolbar);
        fabQuestionMarkHint.setOnClickListener(this);

        startBtn = (Button) findViewById(R.id.startRouteBtn);
        savedBtn = (Button) findViewById(R.id.savedRoutesBtn);
        SettingsBtn = (Button) findViewById(R.id.settingsBtn);
        newBtn = (Button) findViewById(R.id.newRoutesBtn);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.homeCoordinatorLayout);
        startBtn.setOnClickListener(this);
        savedBtn.setOnClickListener(this);
        newBtn.setOnClickListener(this);
        SettingsBtn.setOnClickListener(this);

        mFirebaseAuth = FirebaseAuth.getInstance();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // signed in
                    //onSignedInInitialization(user.getDisplayName());
                    //Toast.makeText(HomeActivity.this, "Welcome"+user.getDisplayName(), Toast.LENGTH_SHORT).show();
                    userName = user.getDisplayName();
                    userEmail = user.getEmail();


                    tvUserWelcome1.setText("Welcome: " + userName);
                    tvUserWelcome2.setBackgroundColor(Color.parseColor(userDefinedColor));
                    tvUserWelcome3.setText("Update Distance: " + minDistanceChangeFromSettings + "m");


                } else {
                    // signed out
                    //OnSignedOutTearingDown();
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setAvailableProviders(Arrays.asList(
                                            new AuthUI.IdpConfig.EmailBuilder().build(),
                                            new AuthUI.IdpConfig.GoogleBuilder().build()))
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Signed-In", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Signing-In Cancelled", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
        if (requestCode == SETTINGS_REQUEST) {
            try {
                userDefinedColor = data.getStringExtra("selectedColorStringKey");
                minDistanceChangeFromSettings = data.getIntExtra("minDistanceChangeFromSettings", 1);

                tvUserWelcome2.setBackgroundColor(Color.parseColor(userDefinedColor));
                tvUserWelcome3.setText("Update Distance: " + minDistanceChangeFromSettings);
            } catch (Exception e) {
                // Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                //sign out
                AuthUI.getInstance().signOut(this);
                break;

            case R.id.exit_menu:
                // exit.
                AuthUI.getInstance().signOut(this);
                Toast.makeText(this, "Thank You for using my App", Toast.LENGTH_SHORT).show();
                this.finishAffinity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startRouteBtn:
                //todo: start button.
                if (checkMobileDataConnectivity()) {
                    startNewRoute();
                } else {
                    createNetErrorDialog();
                }
                break;
            case R.id.newRoutesBtn:
                //todo: show new routes here.
                if (checkMobileDataConnectivity()) {
                    Intent newIntent = new Intent(HomeActivity.this, NewFootsMapActivity.class);
                    startActivity(newIntent);
                } else {
                    createNetErrorDialog();
                }

                break;
            case R.id.savedRoutesBtn:
                //todo: show saved routes
                Intent savedRoutesIntent = new Intent(HomeActivity.this, SavedRoutesActivity.class);
                savedRoutesIntent.putExtra("emailKey", userEmail);
                startActivity(savedRoutesIntent);
                //Toast.makeText(this, "Coming soon by P&P", Toast.LENGTH_SHORT).show();
                break;

            case R.id.settingsBtn:
                //todo: show settings.
                Intent settingsIntent = new Intent(HomeActivity.this, MySettingsActivity.class);
                startActivityForResult(settingsIntent, SETTINGS_REQUEST);
                break;

            case R.id.fabQuestionMarkHint:
                //todo: here place the popup to educate the user on the concerned topic.
                educateUserAboutTheApp();
                break;
        }
    }

    private void educateUserAboutTheApp() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.educate_user_layout, null);
        dialogBuilder.setView(dialogView)
                .setTitle("Developer Talks (Useful)")
                .setPositiveButton("Got It!",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }
                );

        TextView educateTextView = (TextView) dialogView.findViewById(R.id.educateUserText);
        educateTextView.setText(R.string.educateUserString);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();


    }


    protected void createNetErrorDialog() {
/*1) Rasta Napo is a mobility app and needs continuous internet access.
2) WiFi can't provide continuous internet with mobility.
3) So we require mobile internet connectivity for your hassle-free experience.*/
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Rasta Napo is a mobility app and it requires Mobile Internet Connectivity over WiFi for your hassle-free experience.")
                .setTitle("Need Cellular Internet Connectivity")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent();
                                intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                                startActivity(intent);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        }
                );
        AlertDialog alert = builder.create();
        alert.show();
    }


    public boolean checkMobileDataConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean) method.invoke(cm);
        } catch (Exception e) {
            Log.i("MOBILE INTERNET ERROR:", e.getMessage());
        } finally {
            return mobileDataEnabled;
        }
    }

    private void startNewRoute() {

        if (Build.VERSION.SDK_INT < 23) {
            // no need to ask permissions.
            Intent mapsIntent = new Intent(HomeActivity.this, MapsActivity.class);
            mapsIntent.putExtra("myUserEmail", userEmail);
            mapsIntent.putExtra("customPathColorKey", userDefinedColor);
            mapsIntent.putExtra("minDistanceChangeFromSettings", minDistanceChangeFromSettings);
            startActivity(mapsIntent);
        } else {
            if (checkAndRequestPermissions()) {
                //If you have already permitted the permission
                //todo: navigate to map from here FE:3.1.2

            }
        }
    }


    private boolean checkAndRequestPermissions() {
        if (isMyPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION) && isMyPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(coordinatorLayout, "All Required Permissions Granted.", Snackbar.LENGTH_SHORT).show();

            Intent mapsIntent = new Intent(HomeActivity.this, MapsActivity.class);
            mapsIntent.putExtra("myUserEmail", userEmail);
            mapsIntent.putExtra("customPathColorKey", userDefinedColor);
            mapsIntent.putExtra("minDistanceChangeFromSettings", minDistanceChangeFromSettings);
            startActivity(mapsIntent);
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        startNewRoute();
                } else {
                    Toast.makeText(this, "Permissions Required !!", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public boolean isMyPermissionGranted(String permission) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //todo:show LATER snackbar (uncomment the below code).
                /*Snackbar snackbar = Snackbar.make(coordinatorLayout, "Permission Needed", Snackbar.LENGTH_LONG);
                snackbar.setAction("Allow", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkAndRequestPermissions();
                    }
                });
                snackbar.show();*/
            }
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {     // wait for 2 seconds until the back button is pressed again.
            super.onBackPressed();
            Toast.makeText(getBaseContext(), "Thank you for using Rasta Napo!", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();
    }
}
