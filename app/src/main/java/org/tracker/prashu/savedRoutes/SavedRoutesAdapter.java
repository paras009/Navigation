package org.tracker.prashu.savedRoutes;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.tracker.prashu.TrackFootsMap.SavedMapActivity;
import org.tracker.prashu.rastaNapo.R;

import java.util.ArrayList;

public class SavedRoutesAdapter extends RecyclerView.Adapter<SavedRoutesAdapter.MyViewHolder> {

    View viewLinear ,viewGrid;
    LayoutInflater inflater;
    Context context;
    ArrayList<SavedRoutesData> arrayList = new ArrayList<>();
    private int identity;
    private boolean isGridLayoutNeeded = false;

    SavedRoutesAdapter(Context context, ArrayList<SavedRoutesData> arrayList, boolean isGridLayoutNeeded) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.arrayList = arrayList;
        this.isGridLayoutNeeded = isGridLayoutNeeded;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        viewLinear = inflater.inflate(R.layout.saved_routes_layout_linear, parent, false);
        viewGrid = inflater.inflate(R.layout.saved_routes_layout_grid, parent, false);
        MyViewHolder myViewHolder;
        if (isGridLayoutNeeded){
             myViewHolder = new MyViewHolder(viewGrid);
        }else {
             myViewHolder = new MyViewHolder(viewLinear);
        }


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {



        if (isGridLayoutNeeded){
            displayData(holder, position);
        }else {
            displayData(holder, position);
            holder.imgMap.setImageResource(R.drawable.splash_icon);
        }

    }

    private void displayData(@NonNull final MyViewHolder holder, final int position){

        final SavedRoutesData dataPojo = arrayList.get(position);
        holder.tvName.setText(dataPojo.getName());
        holder.tvDate.setText(dataPojo.getDate());
        holder.tvTime.setText(dataPojo.getTime());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SavedMapActivity.class);
                intent.putExtra("id", dataPojo.getId());
                intent.putExtra("name", dataPojo.getName());
                intent.putExtra("startLat", dataPojo.getStart_lat());
                intent.putExtra("startLon", dataPojo.getStart_lon());
                intent.putExtra("currentLat", dataPojo.getCurrent_lat());
                intent.putExtra("currentLon", dataPojo.getCurrent_lon());
                intent.putExtra("polylineArray", dataPojo.getPolyline_array());
                intent.putExtra("duration", dataPojo.getDuration());
                intent.putExtra("distance", dataPojo.getDistance());
                intent.putExtra("color", dataPojo.getColor());
                context.startActivity(intent);
            }
        });


        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setIdentity(dataPojo.getId());
                holder.cardView.setBackgroundColor(Color.parseColor("#FFAB91"));
                return false;
            }
        });
    }

    @Override
    public void onViewRecycled(@NonNull MyViewHolder holder) {
        holder.cardView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public int getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = Integer.parseInt(identity);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        TextView tvName;
        TextView tvDate;
        TextView tvTime;
        ImageView imgMap;
        CardView cardView;

        public MyViewHolder(View v) {
            super(v);

            tvName = (TextView) v.findViewById(R.id.tvNameSavedRoute);
            tvDate = (TextView) v.findViewById(R.id.tvDateSavedRoute);
            tvTime = (TextView) v.findViewById(R.id.tvTimeSavedRoute);
            imgMap = (ImageView) v.findViewById(R.id.imgSavedRoute);
            cardView = (CardView) v.findViewById(R.id.savedMapCardView);
            v.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.add(Menu.NONE, R.id.deleteRoute,
                    Menu.NONE, "Delete Route");
        }
    }
}
