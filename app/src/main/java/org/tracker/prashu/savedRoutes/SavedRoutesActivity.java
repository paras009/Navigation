package org.tracker.prashu.savedRoutes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.tracker.prashu.rastaNapo.MyDatabaseHelper;
import org.tracker.prashu.rastaNapo.R;

import java.util.ArrayList;

public class SavedRoutesActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView savedRoutesRecyclerView;
    SavedRoutesAdapter adapter;
    ArrayList<SavedRoutesData> arrayList;
    MyDatabaseHelper databaseHelper;
    SQLiteDatabase db, db2;
    String currentUserEmail;
    private Toolbar mySavedToolbar;
    FloatingActionButton swapLayoutFloatButton;
    public boolean isGridLayoutManagerNeeded = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_routes);
        Toast.makeText(this, "saved Routes", Toast.LENGTH_SHORT).show();

        swapLayoutFloatButton = (FloatingActionButton) findViewById(R.id.swapLayoutFloatButton);
        swapLayoutFloatButton.setOnClickListener(this);

        mySavedToolbar = (Toolbar) findViewById(R.id.app_bar_saved);
        mySavedToolbar.setTitle("Saved Routes");
        mySavedToolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(mySavedToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentUserEmail = getIntent().getStringExtra("emailKey");
        savedRoutesRecyclerView = (RecyclerView) findViewById(R.id.savedRoutesRecyclerView);

        showDataForSpecificUser();
        registerForContextMenu(savedRoutesRecyclerView);
    }

    public void showDataForSpecificUser() {

        databaseHelper = new MyDatabaseHelper(SavedRoutesActivity.this);
        db = databaseHelper.getWritableDatabase();

        String[] columns = {
                MyDatabaseHelper.ID,
                MyDatabaseHelper.EMAIL,
                MyDatabaseHelper.NAME,
                MyDatabaseHelper.DATE,
                MyDatabaseHelper.TIME,
                MyDatabaseHelper.START_LAT,
                MyDatabaseHelper.START_LONG,
                MyDatabaseHelper.CURRENT_LAT,
                MyDatabaseHelper.CURRENT_LONG,
                MyDatabaseHelper.POLYLINE_ARRAY,
                MyDatabaseHelper.DURATION,
                MyDatabaseHelper.DISTANCE,
                MyDatabaseHelper.COLOR
        };

        Cursor cursor = db.query(MyDatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        arrayList = new ArrayList<>();

        int i1 = cursor.getColumnIndex(MyDatabaseHelper.ID);
        int i2 = cursor.getColumnIndex(MyDatabaseHelper.EMAIL);
        int i3 = cursor.getColumnIndex(MyDatabaseHelper.NAME);
        int i4 = cursor.getColumnIndex(MyDatabaseHelper.DATE);
        int i5 = cursor.getColumnIndex(MyDatabaseHelper.TIME);
        int i6 = cursor.getColumnIndex(MyDatabaseHelper.START_LAT);
        int i7 = cursor.getColumnIndex(MyDatabaseHelper.START_LONG);
        int i8 = cursor.getColumnIndex(MyDatabaseHelper.CURRENT_LAT);
        int i9 = cursor.getColumnIndex(MyDatabaseHelper.CURRENT_LONG);
        int i10 = cursor.getColumnIndex(MyDatabaseHelper.POLYLINE_ARRAY);
        int i11 = cursor.getColumnIndex(MyDatabaseHelper.DURATION);
        int i12 = cursor.getColumnIndex(MyDatabaseHelper.DISTANCE);
        int i13 = cursor.getColumnIndex(MyDatabaseHelper.COLOR);


        cursor.moveToFirst();
        cursor.moveToPrevious();
        while (cursor.moveToNext()) {
            if (cursor.getString(i2).equals(currentUserEmail)) {
                String id = cursor.getString(i1);
                String email = cursor.getString(i2);
                String name = cursor.getString(i3);
                String date = cursor.getString(i4);
                String time = cursor.getString(i5);
                String start_lat = cursor.getString(i6);
                String start_long = cursor.getString(i7);
                String current_lat = cursor.getString(i8);
                String current_long = cursor.getString(i9);
                String polyline_array = cursor.getString(i10);
                String duration = cursor.getString(i11);
                String distance = cursor.getString(i12);
                String color = cursor.getString(i13);

                arrayList.add(new SavedRoutesData(id, email, name, date, time, start_lat, start_long, current_lat, current_long, polyline_array, duration, distance, color));
            }
        }

        if (arrayList.isEmpty()) {
            Toast.makeText(this, "List Empty", Toast.LENGTH_SHORT).show();
        }

        adapter = new SavedRoutesAdapter(SavedRoutesActivity.this, arrayList, isGridLayoutManagerNeeded);
        savedRoutesRecyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        if (isGridLayoutManagerNeeded) {
            savedRoutesRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        } else {
            savedRoutesRecyclerView.setLayoutManager(linearLayoutManager);
        }

        db.close();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int identity = -1;
        try {
            identity = ((SavedRoutesAdapter) savedRoutesRecyclerView.getAdapter()).getIdentity();
        } catch (Exception e) {
            Log.d("gsdfg", e.getLocalizedMessage(), e);
            return super.onContextItemSelected(item);
        }

        switch (item.getItemId()) {
            case R.id.deleteRoute:
                // todo: delete from database and show list again

                if (deleteData(identity)) {
                    showDataForSpecificUser();
                    Toast.makeText(this, "Route Deleted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Deletion Error", Toast.LENGTH_SHORT).show();
                }
                break;

        }
        return super.onContextItemSelected(item);
    }


    public boolean deleteData(int identity) {
        databaseHelper = new MyDatabaseHelper(SavedRoutesActivity.this);
        db2 = databaseHelper.getWritableDatabase();

        String[] deleteCols = new String[]{MyDatabaseHelper.ID};
        db2.delete(MyDatabaseHelper.TABLE_NAME, MyDatabaseHelper.ID + "=" + identity, null);
        db2.close();
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.swapLayoutFloatButton) {
            //Toast.makeText(this, "layout changed", Toast.LENGTH_SHORT).show();

            if (isGridLayoutManagerNeeded) {
                isGridLayoutManagerNeeded = false;
            } else if (!isGridLayoutManagerNeeded) {
                isGridLayoutManagerNeeded = true;
            }

            showDataForSpecificUser();

        }
    }
}
